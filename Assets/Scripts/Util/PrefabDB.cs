﻿using UnityEngine;

[CreateAssetMenu (menuName="Prefab DB", fileName ="Prefabs")]
public class PrefabDB : ScriptableObject
{
    [SerializeField] private GameObject _cell;
    public GameObject Cell { get { return _cell; } }
    
    [SerializeField] private GameObject _cursor;
    public GameObject Cursor { get { return _cursor; } }
}