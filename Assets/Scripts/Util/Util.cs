﻿using UnityEngine;

public static class Util
{
    public static Vector3 RandomPointInView(float z = 0)
    {
        var p = Camera.main.ViewportToWorldPoint(new Vector3(Random.value, Random.value, 0));
        p.z = z;
        return p;
    }
}