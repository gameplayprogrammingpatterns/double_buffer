﻿
using UnityEngine;

public static class Services
{
    public static EventManager Events { get; private set; }
    public static InputManager Input { get; private set; }
    public static PrefabDB Prefabs { get; private set; }
    public static Config Config { get; private set; }

    public static void Init()
    {
        Events = new EventManager();
        Input = new InputManager();
        Prefabs = Resources.Load<PrefabDB>("Prefabs/Prefabs");
        Config = Resources.Load<Config>("Config/Config");
    }
}
