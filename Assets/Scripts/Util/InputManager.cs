﻿
using UnityEngine;

public class InputManager
{
    private static readonly int[] _keycodesAsInts = (int[])System.Enum.GetValues(typeof(KeyCode));

    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Services.Events.Fire(new MouseDownEvent(0, Input.mousePosition));
        }
        else if (Input.GetMouseButtonUp(0))
        {
            Services.Events.Fire(new MouseUpEvent(0, Input.mousePosition));
        }
        
        if (Input.GetMouseButtonDown(1))
        {
            Services.Events.Fire(new MouseDownEvent(1, Input.mousePosition));
        } 
        else if (Input.GetMouseButtonUp(1))
        {
            Services.Events.Fire(new MouseUpEvent(1, Input.mousePosition));
        }

        foreach (var intCode in _keycodesAsInts)
        {
            var code = (KeyCode) intCode;
            if (Input.GetKeyDown(code))
            {
                Services.Events.Fire(new KeyDownEvent(code));
            }
            else if (Input.GetKeyUp(code))
            {
                Services.Events.Fire(new KeyUpEvent(code));
            }
        }
    }

    public Vector3 GetWorldMousePos()
    {
        var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        return pos;
    }

}

public class MouseEvent : GameEvent
{
    public readonly int button;
    public readonly Vector3 mousePos;

    protected MouseEvent(int button, Vector3 mousePos)
    {
        this.button = button;
        this.mousePos = mousePos;
    }
}

public class MouseDownEvent : MouseEvent
{
    public MouseDownEvent(int button, Vector3 mousePos) : base(button, mousePos) {}
}

public class MouseUpEvent : MouseEvent
{
    public MouseUpEvent(int button, Vector3 mousePos) : base(button, mousePos) {}
}


public class KeyEvent : GameEvent
{
    public readonly KeyCode code;

    protected KeyEvent(KeyCode code)
    {
        this.code = code;
    }
}

public class KeyDownEvent : KeyEvent
{
    public KeyDownEvent(KeyCode code) : base(code) {}
}

public class KeyUpEvent : KeyEvent
{
    public KeyUpEvent(KeyCode code) : base(code) {}
}
