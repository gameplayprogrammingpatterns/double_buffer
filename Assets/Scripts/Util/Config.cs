﻿using UnityEngine;

[CreateAssetMenu (menuName="Config", fileName ="Config")]
public class Config : ScriptableObject
{
    [SerializeField] private float _tickInterval;
    public float TickInterval
    {
        get { return _tickInterval; }
    }

    [SerializeField] private Color _pauseColor;
    public Color PauseColor
    {
        get { return _pauseColor; }
    }

    [SerializeField] private Color _activeColor;
    public Color ActiveColor
    {
        get { return _activeColor; }
    }

    [SerializeField] private int _scale;
    public int Scale
    {
        get { return _scale; }
    }

}