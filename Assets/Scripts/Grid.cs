﻿using UnityEngine;

public class Grid : MonoBehaviour
{
    private GameObject _cursor;

    private readonly TaskQueue _tasks = new TaskQueue();

    private int _width, _height;

    private bool[,] _cellState;
    private Cell[,] _cells;

    private static readonly int[][] neighborOffsets =
    {
        new []{-1,  1}, // Top left
        new []{ 0,  1}, // Top
        new []{ 1,  1}, // Top right
        new []{-1,  0}, // Center left
        new []{ 1,  0}, // Center right
        new []{-1, -1}, // Bottom left
        new []{ 0, -1}, // Bottom
        new []{ 1, -1}, // Bottom right
    };

    private bool _isRunning;

    ////////////////////////////////////////////////////////////////////
    // UNITY LIFECYCLE
    ////////////////////////////////////////////////////////////////////
    private void Start()
    {
        _width = 16 * Services.Config.Scale;
        _height = 9 * Services.Config.Scale;

        InitCells();
        SetBackgroundColor();

        _cursor = Instantiate(Services.Prefabs.Cursor);
        _cursor.transform.SetParent(transform, false);

        Services.Events.AddHandler<KeyDownEvent>(OnKeyDown);

        _tasks.Add(new Run(this));
    }

    private void OnDestroy()
    {
        Services.Events.RemoveHandler<KeyDownEvent>(OnKeyDown);
    }

    private void Update()
    {
        UpdateCursor();
        _tasks.Update();
    }

    private void InitCells()
    {
        _cellState = new bool[_width, _height];
        _cells = new Cell[_width, _height];

        for (var x = 0; x < _width; x++)
        {
            for (var y = 0; y < _height; y++)
            {
                var cell = Instantiate(Services.Prefabs.Cell);
                cell.transform.SetParent(transform, false);
                cell.transform.localPosition = new Vector3(x, y, 0);
                var cs = cell.GetComponent<Cell>();
                _cells[x, y] = cs;
                _cellState[x, y] = false;
            }
        }
    }

    ////////////////////////////////////////////////////////////////////
    // UI
    ////////////////////////////////////////////////////////////////////
    private void SetBackgroundColor()
    {
        Camera.main.backgroundColor = _isRunning ? Services.Config.ActiveColor : Services.Config.PauseColor;
    }

    private void UpdateCursor()
    {
        // Snap the cursor to grid coordinates
        var p = Services.Input.GetWorldMousePos();
        p.x = Mathf.Floor(p.x);
        p.y = Mathf.Floor(p.y);
        p.z = transform.localPosition.z;
        _cursor.transform.localPosition = p;

        if (Input.GetMouseButtonDown(0))
        {
            ActivateCell((int)p.x, (int)p.y);
        }
    }

    private void OnKeyDown(KeyDownEvent e)
    {
        if (e.code == KeyCode.Space)
        {
            _isRunning = !_isRunning;
            SetBackgroundColor();
        }
    }


    ////////////////////////////////////////////////////////////////////
    // SIMULATION LOGIC
    ////////////////////////////////////////////////////////////////////
    private void TickSimulation()
    {
        if (!_isRunning) return;

        for (var x = 0; x < _width; x++)
        {
            for (var y = 0; y < _height; y++)
            {
                var numNeighbors = GetNeighborCount(x, y);

                if (_cellState[x, y])
                {
                    switch (numNeighbors)
                    {
                        // Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
                        case 0:
                        case 1:
                            DeactivateCell(x, y);
                            break;

                        // Any live cell with two or three live neighbours lives on to the next generation.
                        case 2:
                        case 3:
                            ActivateCell(x, y);
                            break;

                        // Any live cell with more than three live neighbours dies, as if by overpopulation.
                        default:
                            DeactivateCell(x, y);
                            break;
                    }
                }
                else
                {
                    // Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
                    if (numNeighbors == 3)
                    {
                        ActivateCell(x, y);
                    }
                }
            }
        }
    }

    private int GetNeighborCount(int x, int y)
    {
        var n = 0;
        foreach (var offset in neighborOffsets)
        {
            var nx = x + offset[0];
            var ny = y + offset[1];

            if (IsInBounds(nx, ny) && _cellState[nx, ny])
            {
                n++;
            }
        }

        return n;
    }


    ////////////////////////////////////////////////////////////////////
    // CELL MANIPULATION
    ////////////////////////////////////////////////////////////////////
    private void ActivateCell(int x, int y)
    {
        if (!_cellState[x, y])
        {
            _cellState[x, y] = true;
            _cells[x, y].Activate();
        }
    }

    private void DeactivateCell(int x, int y)
    {
        if (_cellState[x, y])
        {
            _cellState[x, y] = false;
            _cells[x, y].Deactivate();
        }
    }

    private bool IsInBounds(int x, int y)
    {
        return x >= 0 && x < _width && y >= 0 && y < _height;
    }


    ////////////////////////////////////////////////////////////////////
    // TASKS
    ////////////////////////////////////////////////////////////////////
    private class Run : Task
    {
        private readonly Grid _grid;
        private float _elapsedTime;

        public Run(Grid grid)
        {
            _grid = grid;
        }

        internal override void Update()
        {
            if (_grid._isRunning)
            {
                if (_elapsedTime >= Services.Config.TickInterval)
                {
                    _grid.TickSimulation();
                    _elapsedTime -= Services.Config.TickInterval;
                }
                _elapsedTime += Time.deltaTime;
            }
        }
    }

}
