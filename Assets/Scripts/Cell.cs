﻿using System;
using UnityEngine;

public class Cell : MonoBehaviour {

    private readonly TaskQueue _tasks = new TaskQueue();

    private static readonly Vector3 _minScale = new Vector3(Vector3.kEpsilon, Vector3.kEpsilon, Vector3.kEpsilon);
    private static readonly Vector3 _maxScale = Vector3.one;

    private void Awake()
    {
        transform.localScale = _minScale;
    }

    private void Update ()
    {
		_tasks.Update();
	}

    public void Activate()
    {
        _tasks.Add(new ScaleAnimation(this, _minScale, _maxScale));
    }

    public void Deactivate()
    {
        _tasks.Add(new ScaleAnimation(this, _maxScale, _minScale));
    }

    private class ScaleAnimation : Task
    {
        private readonly Cell _cell;
        private float _elapsedTime;
        private readonly Vector3 _startScale;
        private readonly Vector3 _endScale;

        private readonly Quaternion _startRotation;
        private readonly Quaternion _endRotation;

        public ScaleAnimation(Cell cell, Vector3 startScale, Vector3 endScale)
        {
            _cell = cell;
            _startScale = startScale;
            _endScale = endScale;

            _startRotation = Quaternion.identity;
            _endRotation = Quaternion.Euler(270, 180, 90);

        }

        internal override void Update()
        {
            var t = Easing.ExpoEaseOut(Mathf.Clamp01(_elapsedTime / Services.Config.TickInterval));
            _cell.transform.localScale = Vector3.Lerp(_startScale, _endScale, t);
            _cell.transform.rotation = Quaternion.Lerp(_startRotation, _endRotation, t);
            if (t == 1)
            {
                SetStatus(TaskStatus.Success);
            }
            else
            {
                _elapsedTime += Time.deltaTime;
            }
        }
    }

}
