﻿using UnityEngine;

public class Main : MonoBehaviour {

    private void Awake()
    {
        CameraUtil.SetupAspectCamera(new CameraUtil.AspectRatio(16, 9), 2);
	    Services.Init();
	}

    private void Update ()
    {
        Services.Input.Update();
        Services.Events.ProcessQueuedEvents();
    }

}
